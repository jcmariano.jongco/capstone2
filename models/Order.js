const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({
	userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: [true, "User ID is required"]
    },

    products: [

    {

       productId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product",
                required: [true, "Product ID is required"]
            },

            quantity: {
                type: Number,
               
            },

            subTotal: {
                type: Number
            }

        }
    ],

     totalAmount: {
         type: Number
     },

     purchasedOn: {
        type: Date,
        default: new Date()
    }
})

//Model
module.exports = mongoose.model("Order", orderSchema);