const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//create product (admin only)
module.exports.addProduct = (req, res) => {

   
    let newProduct = new Product({
        productName : req.body.name,
        description : req.body.description,
        price : req.body.price
    });

    
    return newProduct.save().then((product, error) => {

 
        if (error) {
            return res.send(false);

 
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};


//Retrieve all products (as admin)
module.exports.getAllProducts = (req,res)=>{
	return Product.find({}).sort({createdOn: -1}).then(result=>{
		return res.send(result)
	})
}

//Retrieve all active products
module.exports.getAllActiveProducts = (req,res)=>{
	return Product.find({isActive:true}).then(result=>{
		return res.send(result)
	})
}

//single product
module.exports.getProduct = (req,res)=>{
	return Product.findById(req.params.productId).then(result=>{
		return res.send(result)
	})
}

//update product information (admin only)
module.exports.updateProduct = (req,res)=>{

	let updatedProduct = {
		productName: req.body.productName,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}

//archive
module.exports.archiveProduct = (req, res) =>{

	return Product.findByIdAndDelete(req.params.productId).then(result=>{
		res.status(204).send()
	}).catch((error) => {
		console.log(error);
		res.status(500).send('Error deleting the product');
	});
}

//activate
module.exports.activateProduct = (req, res) =>{

	return Product.findByIdAndUpdate(req.params.productId, {isActive: true}).then(result=>{
		return res.send(true)
	})
}

