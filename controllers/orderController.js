const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
// Non-admin User checkout
module.exports.checkout = async(req,res) => {

  let orderProducts = []
  let totalAmount = 0

  for(let i=0;i<req.body.products.length;i++)
      {
        let{productName,quantity} = req.body.products[i]
        console.log(productName)
        console.log(quantity)
        const product = await Product.findOne({ productName: { $regex: new RegExp(productName, 'i') }, isActive: true });
        if (!product) {
            return res.status(400).json({ error: `Product ${productName} not found or not active.` });
          }

        totalAmount += product.price * quantity;

        console.log(product.id)

        orderProducts.push({ productId: product._id, productName, quantity });
      }

  let newOrder = new Order({

    userId: req.user.id,
    products: orderProducts,
    totalAmount,
    purchasedOn: new Date()

  })

  return newOrder.save().then((order,err)=>{

    if(err){
      return res.send(false);
    }else{
      return res.send(true);
    }
  })
  .catch(err => res.send(err))
}


module.exports.getAllOrders = async(req, res) => {
  try {
    const orders = await Order.find();

    res.json(orders)
  } catch (error) {
    console.error(error);
    res.status(500).json({error: 'Internal Server error'});
  }
}