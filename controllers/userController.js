const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//user registration
module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email:reqbody.email,
		mobileNo:reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

//user authentication
module.exports.loginUser = (req,res)=>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false;
		}else{
			//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			//if pw match
			if(isPasswordCorrect){
				console.log('sss', result)
				return res.send({access: auth.createAccessToken(result), email: req.body.email, firstname: result.firstName, id: result._id})
			}
			//else pw does not match
			else{
				return res.send(false);
			}


		}
	})


	.catch(err=>res.send(err))
}

// Retrieve user details
module.exports.getProfile = (req, res) => {

    return User.findById(req.user.id)
    .then(result => {
        result.password = "";
        return res.send(result);
    })
    .catch(err => res.send(err))

};
