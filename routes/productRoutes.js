const express = require("express");
const productController = require("../controllers/productController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

//create product (admin only)
router.post("/addproduct", verify, verifyAdmin, productController.addProduct);

//Retrieve all products (as admin)
router.get("/retrieveproduct", verify, verifyAdmin, productController.getAllProducts);

//Retrieve all active products
router.get("/active-product", productController.getAllActiveProducts);

//single product
router.get("/:productId", productController.getProduct);

//update product information (admin only)
router.put("/:productId", productController.updateProduct);

//archive
router.delete("/archive/:productId",verify,verifyAdmin,productController.archiveProduct);

//activate
router.put("/:productId/activate",verify,verifyAdmin,productController.activateProduct);


module.exports = router;