const express = require("express");
const orderController = require("../controllers/orderController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

// Non-admin User checkout
router.post("/checkout", verify, orderController.checkout);
router.get("/getorders", verify, orderController.getAllOrders);

module.exports = router;