const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

//user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//user authentication
router.post("/login",userController.loginUser);

// Retrieve User Details
router.get("/details", verify, userController.getProfile)

module.exports = router;